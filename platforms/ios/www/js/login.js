function logueo() {

    var deviceType = (navigator.userAgent.match(/iPad/i)) == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i)) == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

    if (deviceType == "Android") {
        cordovaHTTP.post("http://sistemascode.com/anecdotas/login.php", {

            usuario: $("#username").val(),
            password: $('#password').val()
        }, {}, function(response) {
            // prints 200
            try {
                data = JSON.parse(response.data);
                if (data.validacion == "ok") {
                    localStorage.setItem("usuario", data.usuario);
                    localStorage.setItem("pk_usuario", data.pk_usuario);

                    $.mobile.changePage("#inicio", {
                        transition: "slideup"
                    });

                } else {

                    alert("Usuario o password incorrecto.");

                }
            } catch (e) {
                alert("Error al conectarse con el servidor.");
            }
        }, function(response) {
            // prints 403
            alert(response.status);

            //prints Permission denied
            alert(response.error);
        });


    } else {
        $.ajax({
            type: "post",
            url: "http://sistemascode.com/anecdotas/login.php",
            data: ({
				usuario: $("#username").val(),
				password: $('#password').val()
            }),
            timeout: 10000,
            crossDomain:true,
            cache: false,
            dataType:"jsonp",
            error: function (xhr, ajaxOptions, thrownError) {
                alert(JSON.stringify(xhr));
                alert(thrownError);
            },
            success: function(data) {
									if (data.validacion == "ok") {
											localStorage.setItem("usuario", data.usuario);
											localStorage.setItem("pk_usuario", data.pk_usuario);

											$.mobile.changePage("#inicio", {
													transition: "slideup"
											});

									} else {

											alert("Usuario o password incorrecto.");

									}
            }
        });

    }
}





function registro() {



    if ($('#Remail').val() != '' && $('#Rpassword').val() != '' && $('#Rnombre').val() != '' && $('#Rapellido').val() != '' && $('#Rpais').val() != '') {

        var deviceType = (navigator.userAgent.match(/iPad/i)) == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i)) == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

        if (deviceType == "Android") {
            cordovaHTTP.post("http://sistemascode.com/anecdotas/registro.php", {
                email: $("#Remail").val(),
                password: $("#Rpassword").val(),
                nombre: $("#Rnombre").val(),
                apellido: $("#Rapellido").val(),
            }, {}, function(response) {
                pais: $("#Rpais").val(),
                // prints 200
                console.log(response.status);
                try {
                    data = JSON.parse(response.data);
                    if (data.validacion == "ok") {

                        localStorage.setItem("usuario", data.usuario);
                        localStorage.setItem("pk_usuario", data.pk_usuario);

                        $.mobile.changePage("#inicio", {
                            transition: "slideup"
                        });


                    } else {
                        console.log(data);
                        alert('El email ya se encuentra en el sistema, utilice otro.');

                    }
                } catch (e) {
                    alert("Error de lectura con el servidor.");
                }
            }, function(response) {
                alert("Error de conexión al servidor");
            });

        }
        else
        {


            $.ajax({
                type: "POST",
                url: "http://sistemascode.com/anecdotas/registro.php",
                data: ({
                    email: $("#Remail").val(),
                    password: $("#Rpassword").val(),
                    nombre: $("#Rnombre").val(),
                    apellido: $("#Rapellido").val(),
                    pais: $("#Rpais").val(),
                }),
                timeout: 10000,
                cache: false,
                crossDomain: true,
                contentType: "application/json",
                dataType: "JSONP",
                success: function(data) {

                    //alert(JSON.stringify(data));
                    if (data.validacion == "ok") {

                        localStorage.setItem("usuario", data.usuario);
                        localStorage.setItem("pk_usuario", data.pk_usuario);

                        $.mobile.changePage("#inicio", {
                            transition: "slideup"
                        });


                    } else {
                        console.log(data);
                        alert('El email ya se encuentra en el sistema, utilice otro.');

                    }

                }
            });
        }



    } else {

        alert("Complete todos los campos");
    }


}
